//
//  LegoDetailPresenterProtocol.swift
//  LegoDetailsFinder
//
//  Created by max kryuchkov on 2/1/19.
//  Copyright © 2019 max kryuchkov. All rights reserved.
//

import UIKit

protocol LegoDetailPresenterProtocol: class {
    
    var wireframe: LegoDetailWireFrameProtocol? {get set}
    var view: LegoDetailViewProtocol? {get set}
    
    //View -> Presenter
    func viewDidLoad()
    func backButtonPressed(from view: UIViewController)
    
}

protocol LegoDetailViewProtocol: class {
    //Presenter -> View
    func showLegoDetail(with detail: LegoDetail)
}

protocol LegoDetailWireFrameProtocol: class {
    func goBackToLegoListView(from view: UIViewController)
}
