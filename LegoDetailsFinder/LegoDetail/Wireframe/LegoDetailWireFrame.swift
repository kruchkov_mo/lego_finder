//
//  LegoDetailWireFrame.swift
//  LegoDetailsFinder
//
//  Created by max kryuchkov on 2/1/19.
//  Copyright © 2019 max kryuchkov. All rights reserved.
//

import UIKit

class LegoDetailWireFrame: LegoDetailWireFrameProtocol {
    
    func goBackToLegoListView(from view: UIViewController) {
        _ = view.navigationController?.popViewController(animated: true)
    }
    
    class func createLegoDetailModule(with legoDetailRef: LegoDetailView, and detail: LegoDetail) {
        let presenter = LegoDetailPresenter()
        presenter.detail = detail
        legoDetailRef.presenter = presenter
        legoDetailRef.presenter?.view = legoDetailRef
        legoDetailRef.presenter?.wireframe = LegoDetailWireFrame()
    }

}
