//
//  LegoDetailPresenter.swift
//  LegoDetailsFinder
//
//  Created by max kryuchkov on 2/1/19.
//  Copyright © 2019 max kryuchkov. All rights reserved.
//

import UIKit

class LegoDetailPresenter: LegoDetailPresenterProtocol {
    
    var view: LegoDetailViewProtocol?
    var wireframe: LegoDetailWireFrameProtocol?
    var detail: LegoDetail?
    
    func viewDidLoad() {
        view?.showLegoDetail(with: detail!)
    }
    
    func backButtonPressed(from view: UIViewController) {
        wireframe?.goBackToLegoListView(from: view)
    }
}
