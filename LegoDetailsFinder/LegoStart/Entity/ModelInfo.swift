//
//  ModelInfo.swift
//  LegoDetailsFinder
//
//  Created by max kryuchkov on 2/1/19.
//  Copyright © 2019 max kryuchkov. All rights reserved.
//

import UIKit

struct ModelInfo {
    var version: Int
    var fileName: String
}
