//
//  LegoStartInteractor.swift
//  LegoDetailsFinder
//
//  Created by max kryuchkov on 2/1/19.
//  Copyright © 2019 max kryuchkov. All rights reserved.
//

import UIKit

class LegoStartInteractor: LegoStartInputInteractorProtocol {
    
    weak var presenter: LegoStartOutputInteractorProtocol?
    
    func checkForUpdate(delegate: DownloadDelegate?) {
        presenter?.progress(value: 0.0)
        presenter?.spinner(hidden: false)
        ApiManager.checkVersion { version in
            self.presenter?.spinner(hidden: true)
            print("Obtained version: \(version)")
            if let localModelVersion = UserDefaultsManager.default.getValue(valueType: Int.self, forKey: .modelVersion) {
                if localModelVersion < version {
                    self.presenter?.progress(hidden: false)
                    ApiManager.downloadModel(modelVersion: version, delegate: delegate)
                    return
                }
                self.presenter?.modelDidFetch(info: ModelInfo(version: version, fileName: "ObjectDetector.mlmodel"))
                return
            }
            self.presenter?.progress(hidden: false)
            ApiManager.downloadModel(modelVersion: version, delegate: delegate)
        }
    }
    
}
