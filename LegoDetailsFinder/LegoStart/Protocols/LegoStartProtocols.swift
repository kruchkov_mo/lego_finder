//
//  LegoStartPresenterProtocol.swift
//  LegoDetailsFinder
//
//  Created by max kryuchkov on 2/1/19.
//  Copyright © 2019 max kryuchkov. All rights reserved.
//

import UIKit

    //View -> Presenter
protocol LegoStartPresenterProtocol: class {
    var wireframe: LegoStartWireFrameProtocol? {get set}
    var view: LegoStartViewProtocol? {get set}
    var interactor: LegoStartInputInteractorProtocol? {get set}
    
    func viewDidLoad()
}

    //Presenter -> View
protocol LegoStartViewProtocol: class {
    func loadingIndicator(hidden: Bool)
    func progressBar(hidden: Bool)
    func progressBar(set: Float)
}

protocol LegoStartWireFrameProtocol: class {
    func showLegoList()
}

    //Presenter -> Interactor
protocol LegoStartInputInteractorProtocol: class {
    var presenter: LegoStartOutputInteractorProtocol? {get set}
    func checkForUpdate(delegate: DownloadDelegate?)
}

    //Interactor -> Presenter
protocol LegoStartOutputInteractorProtocol: class {
    func modelDidFetch(info: ModelInfo)
    func progress(value: Float)
    func progress(hidden: Bool)
    func spinner(hidden: Bool)
}
