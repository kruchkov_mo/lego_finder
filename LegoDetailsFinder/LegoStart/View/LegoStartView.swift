//
//  LegoStartView.swift
//  LegoDetailsFinder
//
//  Created by max kryuchkov on 2/1/19.
//  Copyright © 2019 max kryuchkov. All rights reserved.
//

import UIKit

class LegoStartView: UIViewController, LegoStartViewProtocol {
    
    @IBOutlet weak var downloadProgress: UIProgressView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    var presenter: LegoStartPresenterProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        LegoStartWireFrame.createLegoStartModule(with: self)
        presenter?.viewDidLoad()
    }
    
    func progressBar(set progress: Float) {
        DispatchQueue.main.async {
            if self.downloadProgress.progress < progress {
                self.downloadProgress.progress = progress
            }
        }
    }
    
    func loadingIndicator(hidden: Bool) {
        DispatchQueue.main.async {
            if hidden {
                self.spinner.stopAnimating()
            } else {
                self.spinner.startAnimating()
            }
            self.spinner.isHidden = hidden
        }
    }
    
    func progressBar(hidden: Bool) {
        DispatchQueue.main.async {
            self.downloadProgress.isHidden = hidden
        }
    }
    
}
