//
//  LegoStartWireFrame.swift
//  LegoDetailsFinder
//
//  Created by max kryuchkov on 2/1/19.
//  Copyright © 2019 max kryuchkov. All rights reserved.
//

import UIKit

class LegoStartWireFrame: LegoStartWireFrameProtocol {
    
    func showLegoList() {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        guard let vc = storyBoard.instantiateViewController(withIdentifier: "ListNavigationVC") as? UINavigationController else { return }
        DispatchQueue.main.async {
            AppDelegate.shared?.window?.rootViewController = vc
        }
    }
    
    class func createLegoStartModule(with legoStartRef: LegoStartView) {
        let presenter: LegoStartPresenterProtocol & LegoStartOutputInteractorProtocol = LegoStartPresenter()
        legoStartRef.presenter = presenter
        legoStartRef.presenter?.view = legoStartRef
        legoStartRef.presenter?.wireframe = LegoStartWireFrame()
  
        legoStartRef.presenter?.interactor = LegoStartInteractor()
        legoStartRef.presenter?.interactor?.presenter = presenter
    }
}
