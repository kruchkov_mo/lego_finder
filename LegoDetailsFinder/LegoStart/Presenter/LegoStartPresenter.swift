//
//  LegoStartPresenter.swift
//  LegoDetailsFinder
//
//  Created by max kryuchkov on 2/1/19.
//  Copyright © 2019 max kryuchkov. All rights reserved.
//

import UIKit

class LegoStartPresenter: LegoStartPresenterProtocol, LegoStartOutputInteractorProtocol {
    
    var interactor: LegoStartInputInteractorProtocol?
    
    weak var view: LegoStartViewProtocol?
    var wireframe: LegoStartWireFrameProtocol?
    
    // MARK: LegoStartPresenterProtocol method
    
    func viewDidLoad() {
        interactor?.checkForUpdate(delegate: self)
    }
    
    // MARK: LegoStartOutputInteractorProtocol methods
    
    func modelDidFetch(info: ModelInfo) {
        _ = ObjectDetector.urlOfModelInThisBundle
        print("LegoStartPresenter -> modelDidFetch \(info)")
        wireframe?.showLegoList()
    }
    
    func progress(value: Float) {
        view?.progressBar(set: value)
    }
    
    func progress(hidden: Bool) {
        view?.progressBar(hidden: hidden)
    }
    
    func spinner(hidden: Bool) {
        view?.loadingIndicator(hidden: hidden)
    }
}

extension LegoStartPresenter: DownloadDelegate {
    
    func downloadProgressUpdate(for progress: Float) {
        self.progress(value: progress)
    }
    
    func downloadFinish(info: ModelInfo) {
        self.modelDidFetch(info: info)
    }
    
}
