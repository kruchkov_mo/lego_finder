//
//  LegoListPresenter.swift
//  LegoDetailsFinder
//
//  Created by max kryuchkov on 2/1/19.
//  Copyright © 2019 max kryuchkov. All rights reserved.
//

import UIKit

class LegoListPresenter: LegoListPresenterProtocol {
    
    var wireframe: LegoListWireFrameProtocol?
    var view: LegoListViewProtocol?
    var interactor: LegoListInputInteractorProtocol?
    var presenter: LegoListPresenterProtocol?
    
    func showLegoDetailSelection(with detail: LegoDetail, from view: UIViewController) {
        wireframe?.pushToLegoDetail(with: detail, from: view)
    }
    
    func viewDidLoad() {
        self.loadLegoList()
    }

    func loadLegoList() {
        interactor?.getLegoDetailsList()
    }
    
}

extension LegoListPresenter: LegoListOutputInteractorProtocol {
    func legoDetailsListDidFetch(detailsList: [LegoDetail]) {
        view?.showLegoDetails(with: detailsList)
    }
}
