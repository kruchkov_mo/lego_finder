//
//  Common.swift
//  LegoDetailsFinder
//
//  Created by max kryuchkov on 2/1/19.
//  Copyright © 2019 max kryuchkov. All rights reserved.
//

import UIKit

class Common: NSObject {
    
    class func generateDataList() -> [[String: String]] {
        return [["name": "lego #1", "image": "lego_1"],
                ["name": "lego #2", "image": "lego_2"],
                ["name": "lego #3", "image": "lego_3"],
                ["name": "lego #4", "image": "lego_4"]]
    }
    
}
