//
//  LegoListProtocols.swift
//  LegoDetailsFinder
//
//  Created by max kryuchkov on 2/1/19.
//  Copyright © 2019 max kryuchkov. All rights reserved.
//

import UIKit

protocol LegoListViewProtocol: class {
    // PRESENTER -> VIEW
    func showLegoDetails(with details: [LegoDetail])
}

protocol LegoListPresenterProtocol: class {
    //View -> Presenter
    var interactor: LegoListInputInteractorProtocol? {get set}
    var view: LegoListViewProtocol? {get set}
    var wireframe: LegoListWireFrameProtocol? {get set}

    func viewDidLoad()
    func showLegoDetailSelection(with detail: LegoDetail, from view: UIViewController)
}

protocol LegoListInputInteractorProtocol: class {
    var presenter: LegoListOutputInteractorProtocol? {get set}
    //Presenter -> Interactor
    func getLegoDetailsList()
}

protocol LegoListOutputInteractorProtocol: class {
    //Interactor -> Presenter
    func legoDetailsListDidFetch(detailsList: [LegoDetail])
}

protocol LegoListWireFrameProtocol: class {
    //Presenter -> Wireframe
    func pushToLegoDetail(with detail: LegoDetail, from view: UIViewController)
    static func createLegoListModule(detailListRef: LegoListView)
}
