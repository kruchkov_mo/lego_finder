//
//  LegoListView.swift
//  LegoDetailsFinder
//
//  Created by max kryuchkov on 2/1/19.
//  Copyright © 2019 max kryuchkov. All rights reserved.
//

import UIKit

class LegoListView: UIViewController, LegoListViewProtocol {
   
    @IBOutlet var detailsTblView: UITableView!
    
    var presenter: LegoListPresenterProtocol?
    var detailsList = [LegoDetail]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        LegoListWireframe.createLegoListModule(detailListRef: self)
        presenter?.viewDidLoad()
    }

    // MARK: LegoListViewProtocol method
    
    func showLegoDetails(with details: [LegoDetail]) {
        detailsList = details
        detailsTblView.reloadData()
    }
}

extension LegoListView: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let detail = detailsList[indexPath.row]
        let c = detailsTblView.dequeueReusableCell(withIdentifier: "legoDetailCell", for: indexPath)
        if let cell = c as? LegoDetailTVCell {
            cell.legoDetailTitle?.text = detail.name
            cell.legoDetailImage.image = detail.image
            return cell
        }
        c.textLabel?.text = detail.name
        return c
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return detailsList.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter?.showLegoDetailSelection(with: detailsList[indexPath.row], from: self)
    }
}

