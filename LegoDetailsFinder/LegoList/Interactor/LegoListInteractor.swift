//
//  LegoListInteractor.swift
//  LegoDetailsFinder
//
//  Created by max kryuchkov on 2/1/19.
//  Copyright © 2019 max kryuchkov. All rights reserved.
//

import UIKit

class LegoListInteractor: LegoListInputInteractorProtocol {
    
    weak var presenter: LegoListOutputInteractorProtocol?
    
    func getLegoDetailsList() {
        presenter?.legoDetailsListDidFetch(detailsList: getAllLegoDetail())
    }
    
    func getAllLegoDetail() -> [LegoDetail] {
        var legoList = [LegoDetail]()
        let allLegoDetail = Common.generateDataList()
        for item in allLegoDetail {
            if let detailName = item["name"], let detailImage = item["image"] {
                legoList.append(LegoDetail(name: detailName, image: UIImage(named: detailImage)))
            }
        }
        return legoList
    }
}
