//
//  LegoListWireframe.swift
//  LegoDetailsFinder
//
//  Created by max kryuchkov on 2/1/19.
//  Copyright © 2019 max kryuchkov. All rights reserved.
//

import UIKit

class LegoListWireframe: LegoListWireFrameProtocol {
    
    static func createLegoListModule(detailListRef: LegoListView) {
        let presenter: LegoListPresenterProtocol & LegoListOutputInteractorProtocol = LegoListPresenter()
        
        detailListRef.presenter = presenter
        detailListRef.presenter?.wireframe = LegoListWireframe()
        detailListRef.presenter?.view = detailListRef
        detailListRef.presenter?.interactor = LegoListInteractor()
        detailListRef.presenter?.interactor?.presenter = presenter
    }
    
    func pushToLegoDetail(with detail: LegoDetail, from view: UIViewController) {
        let legoDetailViewController = view.storyboard?.instantiateViewController(withIdentifier: "LegoDetailView") as! LegoDetailView
        LegoDetailWireFrame.createLegoDetailModule(with: legoDetailViewController, and: detail)
        view.navigationController?.pushViewController(legoDetailViewController, animated: true)
    }
}
