//
// ObjectDetector.swift
//
// This file was automatically generated and should not be edited.
//

import CoreML

/// Model Prediction Input Type
@available(macOS 10.14, iOS 12.0, tvOS 12.0, watchOS 5.0, *)
class ObjectDetectorInput : MLFeatureProvider {
    
    /// Input image as color (kCVPixelFormatType_32BGRA) image buffer, 416 pixels wide by 416 pixels high
    var image: CVPixelBuffer
    
    /// (optional) IOU Threshold override (default: 0.45) as double value
    var iouThreshold: Double
    
    /// (optional) Confidence Threshold override (default: 0.25) as double value
    var confidenceThreshold: Double
    
    var featureNames: Set<String> {
        get {
            return ["image", "iouThreshold", "confidenceThreshold"]
        }
    }
    
    func featureValue(for featureName: String) -> MLFeatureValue? {
        if (featureName == "image") {
            return MLFeatureValue(pixelBuffer: image)
        }
        if (featureName == "iouThreshold") {
            return MLFeatureValue(double: iouThreshold)
        }
        if (featureName == "confidenceThreshold") {
            return MLFeatureValue(double: confidenceThreshold)
        }
        return nil
    }
    
    init(image: CVPixelBuffer, iouThreshold: Double, confidenceThreshold: Double) {
        self.image = image
        self.iouThreshold = iouThreshold
        self.confidenceThreshold = confidenceThreshold
    }
}

/// Model Prediction Output Type
@available(macOS 10.14, iOS 12.0, tvOS 12.0, watchOS 5.0, *)
class ObjectDetectorOutput : MLFeatureProvider {
    
    /// Source provided by CoreML
    
    private let provider : MLFeatureProvider
    
    
    /// Boxes × Class confidence (see user-defined metadata "classes") as multidimensional array of doubles
    lazy var confidence: MLMultiArray = {
        [unowned self] in return self.provider.featureValue(for: "confidence")!.multiArrayValue
        }()!
    
    /// Boxes × [x, y, width, height] (relative to image size) as multidimensional array of doubles
    lazy var coordinates: MLMultiArray = {
        [unowned self] in return self.provider.featureValue(for: "coordinates")!.multiArrayValue
        }()!
    
    var featureNames: Set<String> {
        return self.provider.featureNames
    }
    
    func featureValue(for featureName: String) -> MLFeatureValue? {
        return self.provider.featureValue(for: featureName)
    }
    
    init(confidence: MLMultiArray, coordinates: MLMultiArray) {
        self.provider = try! MLDictionaryFeatureProvider(dictionary: ["confidence" : MLFeatureValue(multiArray: confidence), "coordinates" : MLFeatureValue(multiArray: coordinates)])
    }
    
    init(features: MLFeatureProvider) {
        self.provider = features
    }
}


/// Class for model loading and prediction
@available(macOS 10.14, iOS 12.0, tvOS 12.0, watchOS 5.0, *)
class ObjectDetector {
    var model: MLModel
    
    // URL of model assuming it was installed in the documents directory
    static var urlOfModelInThisBundle: URL? = {
        guard var ml = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return nil }
        ml.appendPathComponent("ObjectDetector")
        ml.appendPathExtension("mlmodel")
        
        guard FileManager.default.fileExists(atPath: ml.path) else { return nil }
        
        return try? MLModel.compileModel(at: ml)
    }()
    
    /**
     Construct a model with explicit path to mlmodelc file
     - parameters:
     - url: the file url of the model
     - throws: an NSError object that describes the problem
     */
    init(contentsOf url: URL) throws {
        self.model = try MLModel(contentsOf: url)
    }
    
    /// Construct a model that automatically loads the model from the app's bundle
    convenience init?() {
        guard let url = type(of: self).urlOfModelInThisBundle else {
            return nil
        }
        try? self.init(contentsOf: url)
    }
    
    /**
     Construct a model with configuration
     - parameters:
     - configuration: the desired model configuration
     - throws: an NSError object that describes the problem
 
    convenience init(configuration: MLModelConfiguration) throws {
        try self.init(contentsOf: type(of:self).urlOfModelInThisBundle!, configuration: configuration)
    }*/
    
    /**
     Construct a model with explicit path to mlmodelc file and configuration
     - parameters:
     - url: the file url of the model
     - configuration: the desired model configuration
     - throws: an NSError object that describes the problem
     */
    init(contentsOf url: URL, configuration: MLModelConfiguration) throws {
        self.model = try MLModel(contentsOf: url, configuration: configuration)
    }
    
    /**
     Make a prediction using the structured interface
     - parameters:
     - input: the input to the prediction as ObjectDetectorInput
     - throws: an NSError object that describes the problem
     - returns: the result of the prediction as ObjectDetectorOutput
     */
    func prediction(input: ObjectDetectorInput) throws -> ObjectDetectorOutput {
        return try self.prediction(input: input, options: MLPredictionOptions())
    }
    
    /**
     Make a prediction using the structured interface
     - parameters:
     - input: the input to the prediction as ObjectDetectorInput
     - options: prediction options
     - throws: an NSError object that describes the problem
     - returns: the result of the prediction as ObjectDetectorOutput
     */
    func prediction(input: ObjectDetectorInput, options: MLPredictionOptions) throws -> ObjectDetectorOutput {
        let outFeatures = try model.prediction(from: input, options:options)
        return ObjectDetectorOutput(features: outFeatures)
    }
    
    /**
     Make a prediction using the convenience interface
     - parameters:
     - image: Input image as color (kCVPixelFormatType_32BGRA) image buffer, 416 pixels wide by 416 pixels high
     - iouThreshold: (optional) IOU Threshold override (default: 0.45) as double value
     - confidenceThreshold: (optional) Confidence Threshold override (default: 0.25) as double value
     - throws: an NSError object that describes the problem
     - returns: the result of the prediction as ObjectDetectorOutput
     */
    func prediction(image: CVPixelBuffer, iouThreshold: Double, confidenceThreshold: Double) throws -> ObjectDetectorOutput {
        let input_ = ObjectDetectorInput(image: image, iouThreshold: iouThreshold, confidenceThreshold: confidenceThreshold)
        return try self.prediction(input: input_)
    }
    
    /**
     Make a batch prediction using the structured interface
     - parameters:
     - inputs: the inputs to the prediction as [ObjectDetectorInput]
     - options: prediction options
     - throws: an NSError object that describes the problem
     - returns: the result of the prediction as [ObjectDetectorOutput]
     */
    func predictions(inputs: [ObjectDetectorInput], options: MLPredictionOptions = MLPredictionOptions()) throws -> [ObjectDetectorOutput] {
        let batchIn = MLArrayBatchProvider(array: inputs)
        let batchOut = try model.predictions(from: batchIn, options: options)
        var results : [ObjectDetectorOutput] = []
        results.reserveCapacity(inputs.count)
        for i in 0..<batchOut.count {
            let outProvider = batchOut.features(at: i)
            let result =  ObjectDetectorOutput(features: outProvider)
            results.append(result)
        }
        return results
    }
}
