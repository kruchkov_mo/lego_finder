//
//  MlModelDownloader.swift
//  LegoFinder
//
//  Created by max kruchkov on 1/26/19.
//  Copyright © 2019 max kruchkov. All rights reserved.
//

import Foundation

protocol DownloadDelegate: class {
    func downloadProgressUpdate(for progress: Float)
    func downloadFinish(info: ModelInfo)
}

class MlModelDownloader: NSObject {
    weak var delegate: DownloadDelegate?
    static let shared = MlModelDownloader()
    private override init() {}
    var downloadingVersion: Int? = 0
}

    // MARK: - URLSessionDownloadDelegate

extension MlModelDownloader: URLSessionDownloadDelegate {
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        if let data = try? Data(contentsOf: location) {
            StorageManager.shared.saveFile(modelVersion: downloadingVersion!, fileName: downloadTask.response?.suggestedFilename ?? "unknown.mlmodel", data: data)
            
            UserDefaultsManager.default.writeValue(downloadingVersion!, forKey: .modelVersion)
            delegate?.downloadFinish(info: ModelInfo(version: downloadingVersion!, fileName: downloadTask.response?.suggestedFilename ?? "unknown.mlmodel"))
        }
    }
    
    func URLSession(session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
    }
    
    func downloadModelWithProgress(download url: String) {
        guard let modelUrl = URL(string: url) else {
            return
        }
        let session = Foundation.URLSession(configuration: .default, delegate: self, delegateQueue: nil)
 
        let downloadTask = session.downloadTask(with: modelUrl)
        
        downloadTask.resume()
    }
}

    // MARK: - URLSessionDelegate
extension MlModelDownloader: URLSessionDelegate {
    
    func urlSessionDidFinishEvents(forBackgroundURLSession session: URLSession) {
        if let completionHandler = AppDelegate.shared.backgroundSessionCompletionHandler {
            AppDelegate.shared.backgroundSessionCompletionHandler = nil
            DispatchQueue.main.async {
                completionHandler()
            }
        }
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64,totalBytesExpectedToWrite: Int64) {
        let progress = Float(totalBytesWritten) / Float(totalBytesExpectedToWrite)
        delegate?.downloadProgressUpdate(for: progress)
    }
}
