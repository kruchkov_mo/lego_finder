//
//  UserDefaultsManager.swift
//  SAS Fit
//
//  Created by max kruchkov on 1/26/19.
//  Copyright © 2019 max kruchkov. All rights reserved.
//

import Foundation

enum UserDefaultKeys: String {
    case modelVersion = "kModelVersion"
}

class UserDefaultsManager {
    
    static let `default` = UserDefaultsManager()
    
    func getValue<T>(valueType: T.Type, forKey: UserDefaultKeys) -> T? {
        guard let value = UserDefaults.standard.value(forKey: forKey.rawValue) as? T else { return nil }
        return value
    }
    
    func writeValue<T>(_ value: T?, forKey: UserDefaultKeys) {
        UserDefaults.standard.set(value, forKey: forKey.rawValue)
    }
    
    func clearAllDefaults() {
        let appDomain = Bundle.main.bundleIdentifier!
        UserDefaults.standard.removePersistentDomain(forName: appDomain)
        UserDefaults.standard.synchronize()
    }
}

