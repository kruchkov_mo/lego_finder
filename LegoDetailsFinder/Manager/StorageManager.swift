//
//  StorageManager.swift
//  LegoFinder
//
//  Created by max kruchkov on 1/26/19.
//  Copyright © 2019 max kruchkov. All rights reserved.
//

import Foundation

class StorageManager {
    
    static let shared = StorageManager()
    private init() {}
    
    @discardableResult
    func saveFile(modelVersion: Int, fileName: String, data: Data?) -> Bool {
        guard let documentsUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else {
            return false
        }
        let destinationUrl = documentsUrl.appendingPathComponent(fileName)
        
        guard let data = data else {
            return false
        }
        
        do {
            try data.write(to: destinationUrl, options: .atomic)
            print("file saved at \(destinationUrl)")
            return true
        } catch {
            print("Error while saving file")
        }
        return false
    }
}
