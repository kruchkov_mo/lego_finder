//
//  NetworkManager.swift
//  LegoFinder
//
//  Created by max kruchkov on 1/26/19.
//  Copyright © 2019 max kruchkov. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class ResponseObject {
    var originalResponse: SwiftyJSON.JSON?
    var httpStatus: Int?
    var error: NSError? = nil
}

class ApiManager {
    
    static let modelversionUrl = "https://blog-app-ios.herokuapp.com/get_model/get_version.php"
    static let modelDownloadUrl = "https://blog-app-ios.herokuapp.com/get_model/get_file.php"
    
    class func checkVersion(complation: @escaping (Int) -> ()) {
        NetworkManager.requestWith(modelversionUrl + "?fake_version=17", reqMethod: .get, arrayParams: nil, headers: nil) { (obj) in
            if let e = obj.error {
                print("Version get error: \(e.domain) [\(e.code)]")
            } else {
                if let v = obj.originalResponse?["version"].int {
                    complation(v)
                    return
                }
            }
            complation(-1)
        }
    }
    
    class func downloadModel(modelVersion: Int, delegate: DownloadDelegate?) {
        MlModelDownloader.shared.delegate = delegate
        MlModelDownloader.shared.downloadingVersion = modelVersion
        MlModelDownloader.shared.downloadModelWithProgress(download: modelDownloadUrl)
    }
}

fileprivate class NetworkManager {
    
    class func isConnectedToInternet() -> Bool {
        return NetworkReachabilityManager()!.isReachable
    }
    
    class func requestWith(_ url: String,
                           reqMethod: HTTPMethod,
                           arrayParams: Parameters?,
                           headers: HTTPHeaders?,
                           handler: @escaping (ResponseObject) -> Void) {
        
        let object = ResponseObject()
        Alamofire.request(url,
                          method: reqMethod,
                          parameters: arrayParams,
                          encoding: URLEncoding(),
                          headers: headers).responseJSON { response in
                            
                            switch response.result {
                                case .success(let JSON):
                                    let jsonS = SwiftyJSON.JSON(JSON)
                                    object.originalResponse = jsonS
                                    handler(object)
                                case .failure(let error):
                                    object.error = error as NSError
                                    handler(object)
                            }
                            
        }
    }
}
